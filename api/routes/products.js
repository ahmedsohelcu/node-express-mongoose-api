const express = require('express');
const mongoose = require('mongoose')
const router = express.Router();
//import product model
const Product = require('../models/product')

//==================================================================
//01 //get All product //success
//==================================================================
// router.get('/', (req, res, next) => {
//     Product.find()
//         .exec()
//         .then(docs => {
//             console.log(docs);
//             // if (docs.length >= 0) {
//             res.status(200).json(docs);
//             // } else {
//             //     res.status(404).json({
//             //         message: 'No entries found'
//             //     })
//             // }
//         })
//         .catch(err => {
//             console.log(err);
//             response.status(500).json({
//                 error: err
//             });
//         })
// });

// -----------------------------------
//more described with meta data for api
// -----------------------------------
router.get('/', (req, res, next) => {
    Product.find()
        .select('name price _id')
        .exec()
        .then(docs => {
            const response = {
                count: docs.length,
                products: docs.map(doc => {
                    return {
                        name: doc.name,
                        price: doc.price,
                        _id: doc._id,
                        request: {
                            type: 'GET',
                            url: 'http://localhost:3000/products/' + doc._id
                        }
                    }
                })
            };
            res.status(200).json(response);
        })
        .catch(err => {
            console.log(err);
            response.status(500).json({
                error: err
            });
        })
});



//==================================================================
//02 create/store product //success
//==================================================================
// router.post('/', (req, res, next) => {
//     const product = new Product({
//         _id: new mongoose.Types.ObjectId(),
//         name: req.body.name,
//         price: req.body.price,
//     });

//     product
//         .save()
//         .then(result => {
//             console.log(result)
//             res.status(201).json({
//                 message: 'product created successfully..',
//                 createdProduct: result
//             })
//         })
//         .catch(err => {
//             console.log(err);
//             res.status(500).json({
//                 error: err
//             })
//         });
// });

// -----------------------------------
//more described with meta data for api
// -----------------------------------
router.post('/', (req, res, next) => {
    const product = new Product({
        _id: new mongoose.Types.ObjectId(),
        name: req.body.name,
        price: req.body.price,
    });

    product
        .save()
        .then(result => {
            console.log(result)
            res.status(201).json({
                message: 'product created successfully..',
                createdProduct: {
                    name: result.name,
                    price: result.price,
                    _id: result._id,
                    request: {
                        type: 'GET',
                        url: 'http://localhost:3000/products/' + result._id
                    }
                }
            })
        })
        .catch(err => {
            console.log(err);
            res.status(500).json({
                error: err
            })
        });
});


//==================================================================
//03 show product by id //success
//==================================================================
// router.get('/:prodId', (req, res, next) => {
//     const id = req.params.prodId;
//     Product.findById(id)
//         .exec()
//         .then(doc => {
//             console.log(doc);
//             if (doc) {
//                 res.status(200).json(doc);
//             } else {
//                 res.status(404).json({ message: "No Valid entry found for provided id" });
//             }
//         })
//         .catch(err => {
//             console.log(err);
//             res.status(500).json({ error: err });
//         });
// });

// -----------------------------------
//more described with meta data for api
// -----------------------------------
router.get('/:prodId', (req, res, next) => {
    const id = req.params.prodId;
    Product.findById(id)
        .select('name price _id')
        .exec()
        .then(doc => {
            if (doc) {
                res.status(200).json({
                    product: doc,
                    requests: {
                        type: 'GET',
                        description: 'Get all products',
                        url: 'http://localhost:3000/products/'
                    }
                });

            } else {
                res.status(404).json({ message: "No Valid entry found for provided id" });
            }
        })
        .catch(err => {
            console.log(err);
            res.status(500).json({ error: err });
        });
});

//==================================================================
//04 update product by id //success
//==================================================================
// router.patch('/:prodId', (req, res, next) => {
//     const id = req.params.prodId;
//     const updateOps = {};
//     for (const ops of req.body) {
//         updateOps[ops.propName] = ops.value;
//     }

//     Product.update({ _id: id }, { $set: updateOps })
//         .exec()
//         .then(result => {
//             console.log(result);
//             res.status(200).json(result);
//         })
// });
// -----------------------------------
//more described with meta data for api
// -----------------------------------
router.patch('/:prodId', (req, res, next) => {
    const id = req.params.prodId;
    const updateOps = {};
    for (const ops of req.body) {
        updateOps[ops.propName] = ops.value;
    }

    Product.update({ _id: id }, { $set: updateOps })
        .exec()
        .then(result => {
            res.status(200).json({
                message: 'Product Updated',
                requests: {
                    type: 'GET',
                    description: 'Get all products',
                    url: 'http://localhost:3000/products/' + id
                }
            });
        })
});

//==================================================================
//05 delete product by id //success
//==================================================================
// router.delete('/:prodId', (req, res, next) => {
//     const id = req.params.prodId;
//     Product.remove({ _id: id })
//         .exec()
//         .then(result => {
//             console.log(result);
//             res.status(200).json(result);

//         }).catch(err => {
//             console.log(err);
//             res.status(500).json({
//                 error: err
//             });
//         });
// });

// -----------------------------------
//more described with meta data for api
// -----------------------------------
router.delete('/:prodId', (req, res, next) => {
    const id = req.params.prodId;
    Product.remove({ _id: id })
        .exec()
        .then(result => {
            res.status(200).json({
                message: 'Product deleted successfully..',
                requests: {
                    type: 'POST',
                    description: 'Store a new product',
                    url: 'http://localhost:3000/products/',
                    body: { name: 'String', price: 'Number' }
                }
            });

        }).catch(err => {
            console.log(err);
            res.status(500).json({
                error: err
            });
        });
});


//=====================================================================
//api list from postman
//=====================================================================
//post-http://localhost:3000/products
// {	
// 	"name":"Latest Product",
// 	"price": "1240"
// }

//get-http://localhost:3000/products/5fc538b3a0ac04201cbafed0
//delete-http://localhost:3000/products/5fc5316f97411e119025875d
//get-http://localhost:3000/products/

//patch-http://localhost:3000/products/5fc538b3a0ac04201cbafed0
// [
// 	{"propName":"price","value":"1500"},
// 	{"propName":"name","value":"Latest updated product"}
// ]
//=====================================================================



module.exports = router;