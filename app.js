const express = require('express');
const morgan = require('morgan');
const app = express();

//============================================================
//import router
//============================================================
const productRoutes = require('./api/routes/products')
const orderRoutes = require('./api/routes/orders');

//============================================================
//import body-parser -> to handle incoming request
//============================================================
const bodyParser = require('body-parser');

//============================================================
//import mongoose database
//============================================================
const mongoose = require('mongoose')

//============================================================
//morgan for log every request and use it before router use
//============================================================
app.use(morgan('dev'));

//============================================================
//Use body-parse a //it shop before route group
//============================================================
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());


//============================================================
//connect with mongoose //connection is not successful...
//============================================================
// mongoose.connect('mongodb://localhost/test', {useNewUrlParser: true}); //for local
// mongoose.connect('mongodb+srv://ahmedsohel:' + process.env.MONGO_ATLAS_PW + '@node-rest-shop.yfskx.mongodb.net/node-rest-shop?retryWrites=true&w=majority', {

mongoose.connect('mongodb+srv://ahmedsohel:' + process.env.MONGO_ATLAS_PW + '@node-rest-shop.yfskx.mongodb.net/node-rest-shop?retryWrites=true&w=majority', {
        useNewUrlParser: true,
        useUnifiedTopology: true
    }).then(() => {
        console.log('connected...')
    })
    .catch((e) => {
        console.log(e);
    });




//============================================================
//Define Cors = Cross Origin Resources Sharing
//Put before route group
//============================================================
app.use((req, res, next) => {
    //for only one specific site
    // res.header('Access-Control-Allow-Origin','http://my-website.com');

    //permission to all
    res.header("Access-Control-Allow-Origin", "*"); //for all kinds of site
    res.header(
        "Access-Control-Allow-Headers",
        "Origin, X-Requested-With, Content-Type, Accept, Authorization"
    ); //for all kinds of site

    if (req.method === 'OPTIONS') {
        res.header('Access-Control-Allow-Methods","PUT, POST, PATCH, DELETE, GET');
        return res.status(200).json({});
    }
    next();
});
//=======================END CORS=============================


//============================================================
//Define Route group 
//============================================================
app.use('/products', productRoutes);
app.use('/orders', orderRoutes);


//============================================================
//default error handling start
//============================================================
app.use((req, res, next) => {
    const error = new Error('Not Found');
    error.status(404);
    next(error)
})

app.use((error, req, res, next) => {
    res.status(error.status || 500);
    res.json({
        error: {
            message: error.message
        }
    });
});
//============================================================
//default error handling end
//============================================================


module.exports = app;